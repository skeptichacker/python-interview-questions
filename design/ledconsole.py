"""
Deisgn and implement a small program that given a number will print its equivalent LED text
in the console using asterisks (*) character.

E.g: if the input is 4 the program will print,

    *   *
    *   *
    *   *
    * * *
        *
        *
        *

if the input is 2 the program will print,       

    * * *
        *
        *
    * * *
    *    
    *    
    * * *


Stage I: Design and show the key data structure for the program and describe the logic (half credit)
Stage II: Implement the program (full credit).

"""



