### Some JSON design questions

These questions test whether the candidate thinks correctly about data and relationships and can how well he can connect relationships to a structure and then adjust it for queries.

### Examples

1. Design a JSON data model for a *Star Wars* character, say Darth Vader. Design a RestFUL API for querying the character. Design the reponse JSON.

[Ref: https://edgecoders.com/restful-apis-vs-graphql-apis-by-example-51cb3d64809a ]

Answer:

Data Model:

    {
      "data": {
        "person": {
          "name": "Darth Vader",
          "birthYear": "41.9BBY",
          "planet": {
            "name": "Tatooine"
          },
          "films": [
            { "title": "A New Hope" },
            { "title": "The Empire Strikes Back" },
            { "title": "Return of the Jedi" },
            { "title": "Revenge of the Sith" }
          ]
        }
      }
      }

Query API:

    GET - /people/{id}

Response JSON:

    {
      "name": "Darth Vader",
      "birthYear": "41.9BBY",
      "planetId": 1
      "filmIds": [1, 2, 3, 6],

    }

Sub-questions:
    1. Design an API and response JSON for querying planet information given person's name ?
    2. Design an API and response JSON for querying person information given film id ? How will you implement this query logic ?

2. Design a JSON model for a hotel room reservation confirmed booking. Implement query APIs and response JSON for,
   a. Get the hotel and room details given booking id.
   b. Find out hotel room amenities given the tier (class) of booking. (Class: Executive, Deluxe, Suite etc).
   c. Query API to list out the name and details of people booked on a certain day in the hotel for a certain class of rooms. How will you implement this query logic ?

