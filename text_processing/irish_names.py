"""
Implement a program (regular expressions prefered) to find Irish names from a piece of text.

It is given to you that Irish Names mostly start with "Mc" or "Mac".

You need to find and print both the first and last names.

if the input is for example,

"I was friends with Arthur McGuinness for a long time when I was in Dublin. We used to hangout with Sean MacAlister the veteran from Iraq over a pint of Guinness at the local bar."

The output should be ["Arthur McGuinness", "Sean MacAlister"]

"""
