"""
Given a list of text files implement a TF/IDF algorithm in pure Python which does,

1. Find most frequent words in a given text file using term frequency (tf).
2. Find the most relevant words across the list of text files using Inverse Document Frequency (idf)

Implement your code across a list of given files in this folder.

Think of how will you write unit tests for this problem and implement a test fixture.

No third-party libraries can be used for this problem.

"""
