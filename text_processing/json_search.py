"""
Given a list of JSON files inside a folder tree in the following schema,

{'name': 'Jason Bourne',
 'personal': { 'address': 'unknown',
               'ssn': 'N.A',
               'aadhar': '168429000891',
               'country': 'U.S.A'},
  'missions': { 'level': { 'blackops': ['mission_id_6571','mission_id_7891', 'mission_1983'],
                           'wetwork': ['mission_id_5678','mission_id_1983'],
                           'security': ['mission_id_90','mission_id_4589','mission_id_1456'] }}}

Write a program that will search for a specific mission id among *any* of the missions.

How will you modify your program if there are a million or more files ?

How will you optimize the program to take care of the condition if the mission level is also given ?

NOTE: A mission id can belong to more than one level.

"""
               
