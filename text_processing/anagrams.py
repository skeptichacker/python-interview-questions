"""
Implement a simple anagram finder. That is given a word it will print all permutations of that word (anagram) which are actual words in the dictionary. Use /usr/share/dict/words as your dictionary.

E.g: given input 'heart' it prints,

['earth', 'hater', 'heart', 'herat']

Your program should work in reasonable <(O(n**2)) where 'n' is the number of letters in the word.
"""
