"""
Write Python code to find number of continuous repetitions (more than 1 occurrence) of any
of the alphabets in a given piece of text. (Forget the grammar!)

E.g: On the text 'The following sentence has no grammar errors' the answer should be 3

Note that 'aaa' and 'aaaa' forms just one repetition of 'a'. Any further repetitions
of the same letter are counted again. So 'aabormaaality' forms just 2 repetitions of 'a'
for example.

"""

import re
import string
from collections import defaultdict

rep_re = re.compile('|'.join(('%s{2,}' % item for item in string.ascii_lowercase)))

def repetitions(text):
    """ A simple implementation """
    
    # get rid of spaces
    text = ''.join(text.split())

    i = 0
    counts = defaultdict(int)
    # showing continuous counts
    cc = 0
    
    while i<len(text)-1:
        c = text[i]
        if text[i+1] == c:
            cc += 1
            if cc == 1:
                counts[c] += 1
        else:
            cc = 0

        i += 1

    print counts
    return sum(counts.values())

def repetitions_re(text):
    # One implementation using regular expressions
    # print rep_re.findall(text)
    return len(rep_re.findall(text.lower()))

if __name__ == "__main__":
    text = 'The following sentence has no grammar errors'
    print repetitions_re(text)
    print repetitions(text)
    text2 = 'I aaam loooving aaand eeeeenjoyinngg thisss!'
    print repetitions_re(text2)
    print repetitions(text2)
