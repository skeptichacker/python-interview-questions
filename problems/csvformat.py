"""
Given a list where each entry is a dictionary as follows.

{'text': 'Neural Networks Are #Learning What to Remember and What to...',
 'url': 'https://www.technologyreview.com/s/609710/neural-networks-are-learning-what-to-remember-and-what-to-forget/',
 'keywords': "neural, connections, learning, skill, hebbian" }

Write Python code which will write it into a CSV formatted file as follows.

TEXT, URL, KEYWORDS

Neural Networks Are #Learning What to Remember and What to..., https://www.technologyreview.com/s/609710/neural-networks-are-learning-what-to-remember-and-what-to-forget/, "neural/connectionslearning/skill/hebbian"

Also write a one-liner to parse the file back to the list in the original form.

How will you change your code if the text itself has a comma in it ?

NOTE: You cannot use any existing csv parsing modules for implementing this.
"""


