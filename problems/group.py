"""

Grouping text strings across two lists (of unequal size) by the first chracter and sorting alphabetically.

Order in which words appear in final list does not matter. In other words the groups
should be sorted, but words inside need not be.

E.g: l1 = ['apple', 'orange', 'grapes', 'mango'], l2=['africa','borneo','macaw','dusseldorf','granada','oslo','tel-aviv']
then one possible output is,

[('apple','africa'), ('borneo',), ('dusseldorf',), ('grapes','granada'), ('mango',), ('orange','oslo'), ('tel-aviv',) ]

(Another is),

[('africa','apple'), ('borneo',), ('dusseldorf',), ('granada','grapes'), ('mango',), ('orange','oslo'), ('tel-aviv',) ]

etc.

How will you modify your code to take care of any number of lists ?
Is it possible to write your code in a one-liner ?


"""
