"""
Implement your own version of the os.walk function which scans a directory, from scratch.

Using this program, implement a version of the 'find' program that prints a list of files
matching a pattern. For this test, just print if the input matches any part of the full path of the
filename.

"""
