"""

What will be the output of the following code in Python2 ?

>>> u'\u2010'.join(('P','y','t','h','o','n'))

And this ?

>>> u'\u2010'.join(('P','y','t','h','o','n')).encode('ascii')

How can you fix the second code ? Assume the ascii encoding is necessary.

And this ?

>>> u'\u2013'.join(('P','y','t','h','o','n')).decode('utf-8')

How will you fix the previous code ?

And this ?

>>> u'\u2013'.join(('P','y','t','h','o','n')).encode('utf-8')

"""




