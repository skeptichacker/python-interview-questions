"""

What happens when you run this program ? Why does this happen ?

1. Exception*
2. Prints 130
3. Prints 230

If answer is (1) is there a way for the inner function to 'see' the X defined
in the outer scope in Python2 ? How will you solve this in Python3 ?

"""


def f(*args):

    X = 200
    def inner():
        global X
        
        y = args[0]
        X += y

        return X

    return inner()


print f(30)

