"""
Predict the output of the two functions ? Is there a difference ?
Explain how each works.

"""

def squares1(n):
    return dict([i, i*i] for i in range(10))

def squares2(n):
    return dict([(i, i*i) for i in range(10)])

print squares1(10)
print squares2(10)
