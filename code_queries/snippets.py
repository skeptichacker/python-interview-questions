"""
Small code analysis problems
Guess what each of these code pieces do and describe one of

a. Output
b. Exception if it happens

Explain your inference.

1.

>>> t=(([], 1, 2), 3)
>>> t[0][0].append(10)
??
>>> t
??

2.
>>> (x for x in range(10))
<generator object <genexpr> at 0x7f2d13600730>
>>> x
??

3.

>>> list  = [[]]*5
>>> list[0].append(10)
>>> list
??
>>> list[1].append(20)
>>> list
??

4.

>>> class C(object): pass
...
<class '__main__.T'>
>>> T=type('T', (C,), {'x': 1, 'y':2})
>>> t=T()
>>> isinstance(t, C)
??
>>> issubclass(T, C)
??
>>> t.x + t.y
??

5.

>>> d = {'a': 1, 'b': [2, 3]}
>>> e = {'a': 2, 'b': [3,4], 'c': 10}
>>> d.setdefault('a', 5)
(Output omitted)
>>> d['a']
??
>>> d.update(e)
>>> d['b']
??
>>> d['c']
??

6.

>> l = range(25)
>>> map(lambda x: sum(x), zip([i/2 for i in l[::-2]], [i/2 for i in l[::-3]]))
??

7.

>>> l = range(1, 7)
>>> reduce(lambda x, y: x*y, l)
??

8.

>> map(lambda x, y: x+y, range(5), range(10))
??

>>> map(lambda x, y: x+y, range(5), range(5, 10))
??

"""
