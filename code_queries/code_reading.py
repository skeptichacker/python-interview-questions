"""
Guess what the following function attempts to do  and guess the output without execution.

# Hint: https://en.wikipedia.org/wiki/Maximum_subarray_problem

Level: Advanced (Only very godo programmers!)

"""

def f(l):
    m1 = m2 = l[0]
    for x in l[1:]:
         m1 = max(x, m1 + x)
         m2 = max(m2, m1)
    return m2

if __name__ == "__main__":
    print f([10, -15, 5, -20, 30, -3, 10])
