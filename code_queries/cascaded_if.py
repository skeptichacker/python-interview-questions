"""

1. Modify this code to replace the cascaded if statements with better Pythonic code.
2. Explain your solution and why you think its better from a code maintenance perspective.
3. How will you modify your solution to accept minor variations of input ? For example US or USA instead of U.S/U.S.A or UK instead of U.K ?

# Answer hint: Use dictionaries for (1). Use regular expressions for (3).

"""

def capital_city(country):
    """ Given a country return its capital city """

    if country.lower() == 'india':
        return 'New Delhi'
    elif country.lower() in ('u.s','u.s.a'):
        return 'Washington DC'
    elif country.lower() == 'germany':
        return 'Berlin'
    elif country.lower() == 'australia':
        return 'Canberra'
    elif country.lower() in ('u.k','england'):
        return 'London'
    else:
        return 'I dont know'
