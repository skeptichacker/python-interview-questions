"""

Guess the output and explain why it happens and suggest the fix.

Hint: Cuz 'row' is reused in rows, reference is retained and output becomes
[[2, 3, 4], [2, 3, 4], [2, 3, 4] instead of the expected
[[0, 1, 2], [1, 2, 3], [2, 3, 4]]

Fix: rows = [[0]*n for i in range(m)] to avoid the reference getting replicated.

"""

def make_matrix(m, n):
    """ Make a matrix of order m x n """

    row = [0]*n
    rows=[row for i in range(m)]
    for i in range(m):
        for j in range(n):
            rows[i][j] = i + j

    return rows


print make_matrix(3, 3)

