"""
The following function implements code for finding continuous repetitions of letters in the alphabet
in a given piece of text.

E.g: On the text 'The following sentence has no grammar errors' the answer should be 3
as there is 1 repeition of 'l', 1 of 'm' and 1 of 'r.

Note that 'aaa' and 'aaaa' forms just one repetition of 'a'. Any further repetitions
of the same letter are counted again.

The given function tries to implement a simple function that does this. However there
is a bug in it which produces incorrect counts. Find the bug and write a fix.

"""

from collections import defaultdict

def repetitions(text):
    """ A simple implementation """
    
    # get rid of spaces
    text = ''.join(text.split())

    i = 0
    counts = defaultdict(int)
    
    while i<len(text)-1:
        c = text[i]
        if text[i+1] == c:
            counts[c] += 1

        i += 1

    return sum(counts.values())

if __name__ == "__main__":
    text = 'I aaam looooving aaand eeeeenjoyinngg thisss!'
    print repetitions(text)
    # Prints 15, answer should be 7 instead
    text2 = 'Whaaaaaat an ideaaa Sirjiii!'
    # Prints 9, answer should be 3 instead  
    print repetitions(text2) 
