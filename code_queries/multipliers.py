"""

What will be the output of the code below? Explain your answer.

How would you modify the definition of multipliers to produce the presumably desired behavior?

"""

def multipliers():
  return [lambda x : i * x for i in range(4)]
    
print [m(2) for m in multipliers()]
