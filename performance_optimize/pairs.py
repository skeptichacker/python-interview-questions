"""
The following piece of code finds out pairs of numbers across 2 integer lists whose
sum equals a given number. The function works at O(n**2)

If the lists are huge (say million in size) the function gets very slow. Implement
a faster version of the function in O(n*log(n)) or O(n).

Example: l1= [6, 8, 10, 2, 3, 9], l2=[11, 1, 4, 3, 7]
pairs(l1, l2, 10) returns [(6,4), (9,1), (3,7)]


"""
import random
import timeit

l1,l2,N=[],[],0

def pairs(l1, l2, n):
    """ Given two sequences and a number find all pairs whose sum equals that number """

    # Takes approx 4 sec per invocation when size of lists cross 10000
    pairs = []
    for x in l1:
        for y in l2:
            if x+y == n:
                pairs.append((x,y))

    return pairs

def data():
    global l1,l2,N

    l1=random.sample(range(1, 1000000), 10000)
    l2=random.sample(range(1, 1000000), 10000)
    N = random.choice(range(250000, 1000000))

### Solutions

def pairs2(l1, l2, n):
    """ Faster implementation using dictionary """

    # Takes approx 2 ms per invocation for list size in 10000 range and
    # increases approx linearly from there!    
    pairs = []
    # Convert l2 to a dictionary
    l2_dict = {item:1 for item in l2}

    for num in l1:
        diff = n - num
        if diff in l2_dict:
            pairs.append((num, diff))

    return pairs

def pairs3(l1, l2, n):
    """ Faster implementation using dictionary - another variation """

    # Takes approx 2 ms per invocation for list size in 10000 range and
    # increases approx linearly from there!    
    pairs = []
    # Convert l2 to a dictionary
    l2_dict = {(n-item):item for item in l2}

    for key in l1:
        if key in l2_dict:
            pairs.append((key,l2_dict[key]))

    return pairs

def unittest():
    l1= [6, 8, 10, 2, 3, 9]
    l2=[11, 1, 4, 3, 7]

    assert pairs(l1,l2,10) == [(6,4),(3,7),(9,1)]
    assert pairs2(l1,l2,10) == [(6,4),(3,7),(9,1)]
    assert pairs3(l1,l2,10) == [(6,4),(3,7),(9,1)]          

    
def test():
    # Takes approx 4 sec per invocation 
    pairs(l1, l2, N)

    
if __name__ == "__main__":
    unittest()
    data()
    # print pairs(l1,l2,N)
    t = timeit.Timer('test()', "from __main__ import test")
    print "%.2f msec/pass" % (1000*t.timeit(number=100)/100)    

    
