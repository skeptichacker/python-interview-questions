"""
Given a list where each entry is a dictionary as follows.

{'text': 'Neural Networks Are #Learning What to Remember and What to...',
 'url': 'https://www.technologyreview.com/s/609710/neural-networks-are-learning-what-to-remember-and-what-to-forget/'
 }


Implement a one-liner to write them into a CSV formatted file as follows.

TEXT, URL

Neural Networks Are #Learning What to Remember and What to..., https://www.technologyreview.com/s/609710/neural-networks-are-learning-what-to-remember-and-what-to-forget/

NOTE: You cannot use any existing csv parsing modules for implementing this.
"""


