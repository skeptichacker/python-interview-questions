"""

Implement factorial of a number in Python in one line using anonymous lambda functions.

"""

f = lambda n: reduce(lambda x, y: x*y, range(1, n+1))
