"""
Implement a one liner to filter out words which are lesser than a given length (say 3) from a text file. Final output should be the text minus all such words.

"""
