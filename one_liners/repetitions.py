"""
Given an input as a text string - say a paragraph, write a one-liner to find words
with at least one letter repeating.

E.g: If input is

"Continuous delivery is very important in agile software development. It replaces manual acceptance testing with automated tests which often speeds up the entire software development life cycle"

Output would be,

['Continuous','delivery','important','development','replaces','manual','acceptance','testing','automated','tests','which','speeds','entire','development','cycle']

(It is ok if words repeat).

How will you modify your code to only find words which have vowels repeating ?
How will you modify your code to only find words where only adjacent letters repeat ?

"""
