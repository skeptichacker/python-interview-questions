"""
Format an integer tuple (r,g,b) value as a color hex string. Give two different approaches if possible
and implement a simple test fixture.

"""

def format_hex(r,g,b):
    return '#{0}{1}{2}'.format(*[hex(item).replace('0x','').zfill(2) for item in (r,g,b)])

def format_hex2(r,g,b):
    return '#' + ''.join(map(lambda x: x.replace('0x','').zfill(2), map(hex, (r,g,b))))

if __name__ == "__main__":
    rgb = (8, 11, 1)
    print(format_hex(*rgb))
    print(format_hex2(*rgb))
