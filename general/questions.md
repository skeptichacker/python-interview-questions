These are a mix of Python and shell questions.

1. Shell: What is the quickest way to add recently modified Python files in a folder (and sub-fodlers) to git (git add) via the shell ?
2. Python & Shell: How will you run a local Python HTTP server on port 8000 in one line on the shell ?
3. Python: How to add a folder dynamically (inside code) to the python path so it can be imported ?
4. Python: How will you encode the string u'Python' to ascii ? How will you do that for the string u'\u2013'.join(('P','y','t','h','o','n')) ('P–y–t–h–o–n') which is separted with the unicode DASH (u'\2013') character without an error ?
5. Python: How do you list functions inside a module ? (Note: only functions not everything)
6. Python: Explain local vs global scope in Python with an example.
7. Python: Implement a simple caching decorator in memory (memoizer) in Python. Demonstrate using a function.

